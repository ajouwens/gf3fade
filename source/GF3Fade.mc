using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;
using Toybox.Math as Math;
using Toybox.Time as Time;
using Toybox.Time.Gregorian as Calendar;
using Toybox.WatchUi as Ui;

class GF3Fade extends Ui.WatchFace {
    var hoursUi = new [5];
    var minutesUi = new [5];
    var colorsUi = new [5];
    var wdColorsUi = new [5];
    var fontsUi = new [5];
    var dayFontsUi = new [5];
    var daysPos = new [5];
    var weekDaysPos = new [5];
    var hoursPos = new [5];
    var minutesPos = new [5];
    var monthsPos = new [5];
    var battPos = new [5];
    var days = new [5];
    var weekDays = new [5];
    var months = new [5];
    var saveDay = -1;
    var saveHour = -1;
    var saveMinute = -1;
    var is24Hour = false;
    var battPerc = 101;
    var cx = 109; // Just fenix3
    var cy = 109;
    var cal = [new [3],new [3],new [3],new [3],new [3]];

    function initialize() {
        colorsUi   = [Gfx.COLOR_LT_GRAY, Gfx.COLOR_DK_GRAY, Gfx.COLOR_BLACK, Gfx.COLOR_DK_GRAY, Gfx.COLOR_LT_GRAY];
        wdColorsUi = [Gfx.COLOR_LT_GRAY, Gfx.COLOR_DK_GRAY, Gfx.COLOR_RED, Gfx.COLOR_DK_GRAY, Gfx.COLOR_LT_GRAY];
        fontsUi    = [Gfx.FONT_NUMBER_MILD, Gfx.FONT_NUMBER_MEDIUM, Gfx.FONT_NUMBER_HOT, Gfx.FONT_NUMBER_MEDIUM, Gfx.FONT_NUMBER_MILD];
        dayFontsUi = [Gfx.FONT_XTINY, Gfx.FONT_TINY, Gfx.FONT_SMALL, Gfx.FONT_TINY, Gfx.FONT_XTINY];
        daysPos    = [ [cx-30, cy-102], [cx-18, cy-90], [cx, cy-80], [cx+18, cy-90], [cx+30, cy-102] ];
        weekDaysPos= [ [cx-90, cy-45], [cx-85, cy-23], [cx-80, cy-2], [cx-85, cy+22], [cx-90, cy+40] ];
        monthsPos  = [ [cx+90, cy-45], [cx+85, cy-23], [cx+80, cy-2], [cx+85, cy+22], [cx+90, cy+40] ];
        battPos    = [ [cx-30, cy+102], [cx-18, cy+90], [cx, cy+80], [cx+18, cy+90], [cx+30, cy+102] ];
        hoursPos   = [ [cx-52, cy-82], [cx-24, cy-51], [cx-5, cy-4], [cx-24, cy+45], [cx-52, cy+80] ];
        minutesPos = [ [cx+52, cy-82], [cx+24, cy-51], [cx+5, cy-4], [cx+24, cy+45], [cx+52, cy+80] ];
        is24Hour   = Sys.getDeviceSettings().is24Hour;
    }

    function onLayout(dc) {
    }

    function onShow() {}

    function onHide() {}

    function onExitSleep() {}

    function onEnterSleep() {}

    function onUpdate(dc) {
        dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_WHITE, Gfx.COLOR_WHITE);
        dc.clear();
        var battPerc = (Sys.getSystemStats().battery).toNumber();
        var clockTime = System.getClockTime();
        var zoneOffset = clockTime.timeZoneOffset / 3600f;
        var hour= clockTime.hour;
        var min = clockTime.min;
        var now = Time.now();
        var info = Calendar.info(now, Time.FORMAT_LONG);
        var currentDate = getCurrentDate(now);
        var batts = getBatteryValues(battPerc);
        var hourUI = GF3Helper.getHourUI(hour, is24Hour);

        if(saveHour != hour) {
            hoursUi = getHours(hour);
            saveHour = hour;
        }

        if(saveMinute != min) {
            minutesUi = getMinutes(min);
            saveMinute = min;
        }

        if(saveDay != info.day) {
            saveDay = info.day;
            cal = getCalendar(currentDate, zoneOffset);
        }

        dc.setColor(Gfx.COLOR_LT_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.drawCircle(cx, cy-130, 50);
        dc.drawCircle(cx, cy+130, 50);
        dc.drawCircle(-60, 109, 109);
        dc.drawCircle(-3, 109, 109);
        dc.drawCircle(221, 109, 109);
        dc.drawCircle(280, 109, 109);

        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        dc.drawText(cx, cy-6, Gfx.FONT_NUMBER_HOT, ":", Gfx.TEXT_JUSTIFY_CENTER | Gfx.TEXT_JUSTIFY_VCENTER);

        for (var i = 0; i < 5; i += 1) {
            drawHourMinute(dc, hoursPos[i], colorsUi[i], fontsUi[i], hoursUi[i], "%d", Gfx.TEXT_JUSTIFY_RIGHT);
            drawHourMinute(dc, minutesPos[i], colorsUi[i], fontsUi[i], minutesUi[i], "%02d", Gfx.TEXT_JUSTIFY_LEFT);
        }

        var text = "";
        for (var i = 0; i < 5; i += 1) {
            text = cal[i][0];
            drawDay(dc, daysPos[i], wdColorsUi[i], dayFontsUi[i], Lang.format("$1$", [text]), Gfx.TEXT_JUSTIFY_CENTER);
            text = cal[i][1];
            drawDay(dc, weekDaysPos[i], wdColorsUi[i], dayFontsUi[i], Lang.format("$1$", [text]), Gfx.TEXT_JUSTIFY_CENTER);
            text = cal[i][2];
            drawDay(dc, monthsPos[i], wdColorsUi[i], dayFontsUi[i], Lang.format("$1$", [text]), Gfx.TEXT_JUSTIFY_CENTER);
            text = batts[i];
            drawDay(dc, battPos[i], wdColorsUi[i], dayFontsUi[i], Lang.format("$1$", [text]), Gfx.TEXT_JUSTIFY_CENTER);
        }

        if (!is24Hour) {
            dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
            dc.drawText(cx-61, cy-32, Gfx.FONT_TINY, hourUI[1] , Gfx.TEXT_JUSTIFY_LEFT);
        }
    }

    function getCalendar(currentDate, zoneOffset) {
        for (var i = 0; i < 5; i += 1) {
            // If the timeZone < 0, then we're 1 day 'behind'. Gregorian.moment() is in UTC, Calendar.info is local time.
            var j = zoneOffset < 0 ? (i-1) : (i-2);
            cal[i][0] = getDay(currentDate, j);
            cal[i][1] = getWeekDay(currentDate, j);
            cal[i][2] = getMonth(currentDate, j);
        }
        return cal;
    }

    function drawHourMinute(dc, position, color, font, text, format, align) {
        dc.setColor(color, Gfx.COLOR_TRANSPARENT);
        dc.drawText(position[0], position[1], font, text.format(format), align | Gfx.TEXT_JUSTIFY_VCENTER);
    }

    function drawDay(dc, position, color, font, text, align) {
        dc.setColor(color, Gfx.COLOR_TRANSPARENT);
        dc.drawText(position[0], position[1], font, text, align | Gfx.TEXT_JUSTIFY_VCENTER);
    }

    function getHours(hour) {
        hoursUi = [hour - 2.0, hour - 1.0, hour + 0.0, hour + 1.0, hour + 2.0];
        for (var i = 0; i < hoursUi.size(); i += 1) {
            if(is24Hour && hoursUi[i] < 0) {
                hoursUi[i] += 24;
            }
            if(is24Hour && hoursUi[i] > 23) {
                hoursUi[i] -= 24;
            }
            if(!is24Hour && hoursUi[i] < 1) {
                hoursUi[i] += 12;
            }
            if(!is24Hour && hoursUi[i] > 12) {
                hoursUi[i] = hoursUi[i].toNumber() % 12;
            }
        }
        return hoursUi;
    }

    function getMinutes(minute) {
        minutesUi = [minute - 2.0, minute - 1.0, minute + 0.0, minute + 1.0, minute + 2.0];
        for (var i = 0; i < hoursUi.size(); i += 1) {
            if(minutesUi[i] < 0) {
                minutesUi[i] += 60;
            }
            if(minutesUi[i] > 59) {
                minutesUi[i] -= 60;
            }
        }
        return minutesUi;
    }

    function getDay(date, offset) {
        var dday = date[2] + offset;
        var options = { :second => 0, :hour => 0, :minute => 0, :year => date[0], :month => date[1], :day => dday};
        var cal = Calendar.moment(options);
        var info = Calendar.info(cal, Time.FORMAT_SHORT);
        return info.day;
    }

    function getWeekDay(date, offset) {
        var dday = date[2] + offset;
        var options = { :second => 0, :hour => 0, :minute => 0, :year => date[0], :month => date[1], :day => dday};
        var cal = Calendar.moment(options);
        var info = Calendar.info(cal, Time.FORMAT_LONG);
        return info.day_of_week;
    }

    function getMonth(date, offset) {
        var mmon = date[1] + offset;
        var options = { :second => 0, :hour => 0, :minute => 0, :year => date[0], :month => mmon, :day => 1};
        var cal = Calendar.moment(options);
        var info = Calendar.info(cal, Time.FORMAT_LONG);
        return info.month;
    }

    function getCurrentDate(now) {
        var today = Calendar.info(now, Time.FORMAT_SHORT);
        var xyear = today.year;
        var xmon  = today.month;
        var xday  = today.day;
        return [xyear, xmon, xday];
    }

    function getBatteryValues(battPerc) {
        if (battPerc == 100) {
            return [98, 99, 100, "", ""];
        } else if (battPerc == 99) {
            return [97, 98, 99, 100, ""];
        } else if (battPerc == 1) {
            return ["", 0, 1, 2, 3];
        } else if (battPerc == 0) {
            return ["", "", 0, 1, 2];
        }
        return [battPerc - 2, battPerc -1, battPerc, battPerc + 1, battPerc + 2];
    }
}
