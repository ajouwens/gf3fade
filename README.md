GF3Fade - Digital watch 
=======================

Description
-----------
Digital watch face, with the date, time and battery "fading' in and out.
--------------
Please, leave your comments, bug reports and ideas on this forum:
https://forums.garmin.com/showthread.php?265723-Watchfaces-GF3-
--------------
Version 1.0:
- First version with gray/black/red colors
--------------
